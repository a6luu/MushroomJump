﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        handleEsc();
	}

    void handleEsc(){
        if (Input.GetKey(KeyCode.Escape)) {
            quitGame();
        }
    }

    public void quitGame() {
        print("Exiting");
        Application.Quit();
    }
}
