﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	// Movement Related Stuff
	public float speed = 10.0f;
	float rotateSpeed = 1.0f;
	public float cameraSpeed = 2.0f;
	public float jumpSpeed = 10.0f;
	public float gravity = 20.0f;
	private Vector3 moveDirection = Vector3.zero;
	CharacterController controller;
	public bool canJump;

	public bool isWalking;

	// Grabbing
	public bool isGrabbing;
	public bool isMovableTriggered;
	public GameObject grabbedObject;

	// Rotate Camera
	public float minY = -45.0f;
	public float maxY = 45.0f;

	public float sensX = 100.0f;
	public float sensY = 100.0f;
	public float rotCameraX;
	public float rotCameraY;

	// Stacking
	public GameObject stackedGameObject;

	public GameManager gameManager;

	// Animator
	Animator anim;

	// Camera
	public GameObject cameraGO;

	// Active Player
	public bool isActivePlayer;

	// Use this for initialization
	void Start () {
		isMovableTriggered = false;
		isWalking = false;
		canJump = true;
		controller = GetComponent<CharacterController>();
		rotCameraX = transform.eulerAngles.y;
		rotCameraY = 0.0f;

		grabbedObject = null;
		stackedGameObject = null;

		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();

		cameraGO = GameObject.FindGameObjectWithTag("CameraContainer");
		cameraGO.transform.localEulerAngles = new Vector3 (0, 0, 0);

		anim = GetComponent<Animator> ();

		if (gameObject.tag == "Player")
			isActivePlayer = true;
		else
			isActivePlayer = false;
	}
	
	// Update is called once per frame
	void Update () {
		handleInput ();
		fixRotation ();
	}

	void move() {
		moveDirection = new Vector3(Input.GetAxis("Horizontal") * speed, moveDirection.y, Input.GetAxis("Vertical") * speed);
		moveDirection = transform.TransformDirection(moveDirection);
		if (controller.isGrounded) {
			if (Input.GetButton ("Jump") && canJump) {
				moveDirection.y = jumpSpeed;
				anim.SetBool ("isJumping", true);
			} else
				anim.SetBool ("isJumping", false);

		}
		moveDirection.y -= gravity * Time.deltaTime;
		controller.Move(moveDirection * Time.deltaTime);

	}

	void rotateCamera() {
		// Rotating works like this: horizontal rotation will rotate the actual player
		// However, camera will rotate only vertically
		// TODO: make this better later

		// For Mouse Rotation
		if (Input.GetMouseButton (0)) {
			rotCameraX += Input.GetAxis ("Mouse X") * sensX * Time.deltaTime;
			rotCameraY += Input.GetAxis ("Mouse Y") * sensY * Time.deltaTime;
			rotCameraY = Mathf.Clamp (rotCameraY, minY, maxY);
			cameraGO.transform.localEulerAngles = new Vector3 (-rotCameraY, 0, 0);
			transform.localEulerAngles = new Vector3 (0, rotCameraX, 0);
		}

		// For Key Rotation
		if (Input.GetKey (KeyCode.L))
			rotCameraX += sensX * Time.deltaTime;
		if (Input.GetKey (KeyCode.J))
			rotCameraX -= sensX * Time.deltaTime;
		if (Input.GetKey (KeyCode.I))
			rotCameraY += sensY * Time.deltaTime;
		if (Input.GetKey (KeyCode.K))
			rotCameraY -= sensY * Time.deltaTime;
		cameraGO.transform.localEulerAngles = new Vector3 (-rotCameraY, 0, 0);
		transform.localEulerAngles = new Vector3 (0, rotCameraX, 0);
	}

	void fixRotation() {
		if (stackedGameObject != null) {
			transform.rotation = new Quaternion (0, 0, 0, 0);
		}
	}

	void handleStack () {
		if (Input.GetKey (KeyCode.Q)) {
			// we wanna break the stack
			transform.parent = GameObject.Find("world").transform;
			if (gameObject.tag == "Player") {
				GameObject[] stackableGameObjects = GameObject.FindGameObjectsWithTag ("Stackable");
				foreach (GameObject stackableGameObject in stackableGameObjects) {
					stackableGameObject.GetComponent<Player> ().enabled = false;
					stackableGameObject.GetComponent<Shroombas> ().enabled = true;
				}
			}
			Quaternion correctRotation = stackedGameObject.transform.rotation;
			moveDirection.y += jumpSpeed;
			moveDirection += stackedGameObject.transform.forward * 5;
			stackedGameObject = null;
			controller.Move(moveDirection * Time.deltaTime);
			transform.rotation = correctRotation;

			// move the camera to be in the bottom of the stack so that we can easily move the camera
			cameraGO.transform.parent = this.transform;
			cameraGO.transform.localPosition = Vector3.zero;
		}
	}

	void handleInput() {
		if (Mathf.Sqrt (moveDirection.x * moveDirection.x + moveDirection.z * moveDirection.z) > 0.1) {
			isWalking = true;
		} else {
			isWalking = false;
		}
		anim.SetBool ("isWalking", isWalking);

		// probs make it a switch statement later
		if (stackedGameObject == null) {
			move ();
			rotateCamera ();
		} else {
			handleStack ();
		}

		// Grabbing
		isGrabbing = false;
		if (Input.GetButton ("Grab") && !isGrabbing) {
			if (grabbedObject != null) {
				grabbedObject.transform.parent = this.transform;
				Debug.Log ("Grabbed object");
			}
			isGrabbing = true;
		} else {
			if (grabbedObject != null) {
				grabbedObject.transform.parent = GameObject.Find ("world").transform;
				Debug.Log ("Dropped object");
			}
			isGrabbing = false;
		}

	}

	void OnControllerColliderHit(ControllerColliderHit hit) {
		if (hit.normal.y <= 0.6) {
			canJump = false;
		} else {
			canJump = true;
		}
			

		// Stacking mechanic
		if (hit.gameObject.tag == "Stackable" && hit.normal.y >= 0.8 && stackedGameObject == null) {
			Debug.Log ("Time to stack");
			stackedGameObject = hit.gameObject;

			// change rotation
			hit.gameObject.transform.eulerAngles = transform.rotation.eulerAngles;

			// change position
			Vector3 pos = stackedGameObject.transform.position;
			transform.position = new Vector3 (pos.x, transform.position.y, pos.z);

			// set this parent to the hit's transform
			transform.parent = hit.transform;

			// move the camera to be in the bottom of the stack so that we can easily move the camera
			cameraGO.transform.parent = stackedGameObject.transform;
			cameraGO.transform.localPosition = Vector3.zero;


			// delegate controls over to hit
			hit.gameObject.GetComponent<CharacterController> ().enabled = true;
			hit.gameObject.GetComponent<Player> ().enabled = true;
			hit.gameObject.GetComponent<Shroombas> ().enabled = false;
			moveDirection = new Vector3 (0, 0, 0);



		}

		if (hit.gameObject.tag == "Star" && stackedGameObject == null) {
			print ("got a star");
			gameManager.collectStar (hit.gameObject);
		}
			
	}

	// Collision Events
	void OnCollisionEnter(Collision other) {
		if (other.gameObject.tag == "Movable") {
			Debug.Log ("Entered Movable");
			grabbedObject = other.gameObject;
		}

	}

	void OnCollisionExit(Collision other) {
		if (other.gameObject.tag == "Movable") {
			Debug.Log ("Exited Movable");
			grabbedObject = null;
		}

	}
}
