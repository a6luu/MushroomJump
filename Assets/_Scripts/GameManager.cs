﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour {
	

	public Text numStarsText;
	public Text timerText;
	public int numStars;
	public int totalStars;
	public float timerSeconds;
	public ChangeScene changeScene;

	HashSet<GameObject> collectedStars = new HashSet<GameObject>();
	GameObject[] stars;


	// Use this for initialization
	void Start () {
		numStars = 0;
		stars = GameObject.FindGameObjectsWithTag ("Star");
		totalStars = stars.Length;
		setStarsText ();
		changeScene = GetComponent<ChangeScene> ();
		timerSeconds = 0;
		
	}
	
	// Update is called once per frame
	void Update () {
		updateTimerText ();
	}

	public void collectStar(GameObject star) {
		collectedStars.Add (star);
		star.SetActive(false);
		numStars = collectedStars.Count;
		setStarsText ();
		if (numStars == totalStars) {
			levelComplete ();
		}
	}

	void setStarsText() {
		numStarsText.text = "Stars: " + numStars.ToString() + " / " + totalStars.ToString();

	}

	void updateTimerText() {
		timerSeconds += Time.deltaTime;
		timerText.text = "Time: " + timerSeconds.ToString("F4");

	}

	void levelComplete() {
		print ("Level Complete");

		bool hasHighScore = PlayerPrefs.HasKey ("highscore");
		float newHighScore;
		if (hasHighScore) {
			newHighScore = PlayerPrefs.GetFloat ("highscore");
			if (timerSeconds < newHighScore) {
				newHighScore = timerSeconds;
			}
		} else {
			newHighScore = timerSeconds;
		}
		PlayerPrefs.SetFloat ("highscore", newHighScore);
		PlayerPrefs.SetFloat ("currentscore", timerSeconds);

		changeScene.change (3);
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			print ("Game Over");
			changeScene.change (2);
		}
	}
}
