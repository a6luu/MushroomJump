﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shroombas : MonoBehaviour {
	public float gravity = 20.0f;
	private Vector3 moveDirection = Vector3.zero;
	CharacterController controller;

	// Use this for initialization
	void Start () {
		controller = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
		if (controller.enabled)
			move ();
	}

	void move() {
		moveDirection.y -= gravity * Time.deltaTime;
		controller.Move(moveDirection * Time.deltaTime);
	}
}
