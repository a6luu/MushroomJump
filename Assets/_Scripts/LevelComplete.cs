﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelComplete : MonoBehaviour {

	public Text highScoreText;
	public Text currentScoreText;

	// Use this for initialization
	void Start () {
		float highScore = PlayerPrefs.GetFloat ("highscore");
		float currentScore = PlayerPrefs.GetFloat ("currentscore");
		highScoreText.text = "High Score: " + highScore;
		currentScoreText.text = "Current Score: " + currentScore;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
